// конец строки - точка с запятой
#pragma semicolon 1

// стандартные инклюды
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <csgo_colors>

// инклюды от людей
//#include <lastrequest>
#include <shop>
#include <sm_jail_redie>
#include <t2lrcfg>
#include <SteamWorks>
//#include <smlib>

// обязательно новый синтаксис
#pragma newdecls required

// версия плагинаы
#define VERSION "1.0.62"
// информация о плагине
public Plugin myinfo =
{
	name 		= "[Jail] Stats",
	author 		= "AlexTheRegent & ShaRen",
	description = "Статистика для JAIL сервера",
	version 	= VERSION,
	url 		= "Servers-Info.Ru"
}

// макросы
// получить userid из index
#define UID(%0) GetClientUserId(%0)
// получить index из userid
#define CID(%0) GetClientOfUserId(%0)
// проверка времени при нажатии в SQL, вернуть Plugin_Handled (для команд)
#define CTH(%0) int iTime=GetTime(); if (iTime<g_iSQLTime[%0]) {g_iSQLTime[%0]=iTime+2; PrintToChat(%0, "Интервал запроса 2 секунды"); return Plugin_Handled;}
// проверка времени при нажатии в SQL, вернуть void (для функций)
#define CT(%0) int iTime=GetTime(); if (iTime<g_iSQLTime[%0]) {g_iSQLTime[%0]=iTime+2; PrintToChat(%0, "Интервал запроса 2 секунды"); return;}

// дебаг сообщение
#define DEBUG
#if defined DEBUG
stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/jail_stats.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)
#else
#define dbgMsg(%0)
#endif

// переменные
#define iCT_COEF		4	// множитель очков КТ
#define iCT2_COEF		4	// множитель очков КТ за сбор Т
#define T_ROUND_COEF	1	// множитель очков Т за раунд
#define T_KILL_COEF		70	// Множитель очков Т за убийства
#define T_MAX			20	// Максимальное кол-во Т на сервере
#define F_BLOOD			2	// Множитель за первое убийство
#define KNIFE_COEF		4	// Множитель за убийство с ножа
#define HE_COEF			6	// Множитель за убийство гранатой
#define MIN_T			5	// Минимум Т для работы плагина

// переменные
Database	g_hDatabase;					// подключение к SQL
Menu		g_hMenu_Main;					// главное меню
Menu		g_hMenu_Info;					// инфо меню
char 		g_szAuth[MAXPLAYERS+1][32];		// steamid клиентов
char		g_szCurrentMap[64];				// текущая карта
bool		g_fMapWinLoseIsSent;			// отправлены ли данные от WinLose
float		g_fMapCoef;						// текущий коэф. карты
bool		g_bJoined[MAXPLAYERS+1];		// игрок был подключен за 30 секунд c начала раунда
											// до истечения 30 секунд все индексы = true (даже не подключенные)
int			g_iTCount30;					// сколько было Т в начале раунда (т.е. те которые играли в этом раунде, а не потом зашли и сидели в спеках)
int			g_iRoundStartTime;				// время начала раунда (для вычисления длительности раунда)
int			g_iGatherPoints;				// сколько дается за сбор Т в конце раунда
int			g_iTDies;						// сколько убито Т за раунд
bool		g_bFirstBlood;					// была ли пролита первая кровь
int			g_iSQLTime[MAXPLAYERS+1];		// время последнего запроса игрока
int			g_iPointsT[MAXPLAYERS+1];		// сколько у игрока очк. за T
int			g_iPointsCT[MAXPLAYERS+1];		// сколько у игрока очк. за CT
int			g_iMapWin;						// общее количество побед на текущей карте
int			g_iMapLose; 					// общее количество поражений на текущей карте
char		g_sCTRanks[21][64];				// названия званий у охраны [0-полное 1-сокращ][количество званий][длинна слов]
char		g_sTRanks[21][64];				// названия званий у зеков [0-полное 1-сокращ][количество званий][длинна слов]

// начало плагина (выполняется один раз за всё время работы сервера, при загрузке)
public void OnPluginStart() 
{
	// подключение к локальной SQL
	char szError[256];
	g_hDatabase = SQLite_UseDatabase("jail_stats", szError, sizeof(szError));
	if ( g_hDatabase == null ) {
		LogError("SQLite_UseDatabase error: \"%s\"", szError);
		SetFailState("SQLite_UseDatabase error: \"%s\"", szError);
	}
	// создание таблиц
	SQL_LockDatabase(g_hDatabase);
	SQL_FastQuery(g_hDatabase, "CREATE TABLE IF NOT EXISTS `maps` (`mapname` VARCHAR(64), `win` INT, `lose` INT);");
	SQL_FastQuery(g_hDatabase, "CREATE TABLE IF NOT EXISTS `clients` (`auth` VARCHAR(32), `ptsT` INT, `ptsCT` INT, `name` VARCHAR(32));");
	SQL_UnlockDatabase(g_hDatabase);
	
	// хук нужных событий
	HookEvent("player_death",		Event_PlayerDeath, EventHookMode_Pre);
	HookEvent("player_spawned",		Event_player_spawned);
	//HookEvent("player_connect",		Event_PlayerConnect);
	HookEvent("round_end",			Event_RoundEnd);
	HookEvent("round_start",		Event_RoundStart, EventHookMode_PostNoCopy);
	HookEvent("cs_win_panel_match",	Event_WinMapMatch, EventHookMode_PostNoCopy);	// вызывается за несколько секунд перед ссменой карты
	
	// создание нужных команд
	RegConsoleCmd("sm_rank", 		Command_Rank);
	RegConsoleCmd("sm_jailstats",	Command_JailStats);
	RegConsoleCmd("sm_jailrank",	Command_JailStats);
	RegConsoleCmd("sm_js",			Command_JailStats);
	RegConsoleCmd("sm_jailinfo",	Command_JailInfo);
	RegConsoleCmd("sm_ji",			Command_JailInfo);
	RegAdminCmd("sm_js_givepts", Command_Give, ADMFLAG_ROOT, "sm_js_givepts <steamid> <team 2-t or 3-ct> <pts> - Give points to player");
	RegAdminCmd("sm_js_getpts", Command_Get, ADMFLAG_ROOT, "sm_js_getepts <steamid>- Get points to player");
	
	// создание главного меню
	g_hMenu_Main = new Menu(Handle_MainMenu);
	g_hMenu_Main.SetTitle("[Jail] Stats");
	g_hMenu_Main.AddItem(NULL_STRING, "Топ КТ");
	g_hMenu_Main.AddItem(NULL_STRING, "Топ Т");
	g_hMenu_Main.AddItem(NULL_STRING, "Топ сложных карт");
	g_hMenu_Main.AddItem(NULL_STRING, "Мой ранг");
	g_hMenu_Main.AddItem(NULL_STRING, "Jail Info");

	// создание инфо меню
	g_hMenu_Info = new Menu(Handle_InfoMenu);
	g_hMenu_Info.SetTitle("[Jail] Info");
	g_hMenu_Info.AddItem(NULL_STRING, "Общая информация");
	g_hMenu_Info.AddItem(NULL_STRING, "Как начисляются очки КТ");
	g_hMenu_Info.AddItem(NULL_STRING, "Как начисляются очки Т");
	g_hMenu_Info.AddItem(NULL_STRING, "Цифры и звездочки в таблице очков TAB");

	CreateTimer(10.0, Update, _, TIMER_REPEAT);

	g_sCTRanks[0]  = "✩";
	g_sCTRanks[1]  = "✩";
	g_sCTRanks[2]  = "✩✩";
	g_sCTRanks[3]  = "✩✩✩";
	g_sCTRanks[4]  = "✬";
	g_sCTRanks[5]  = "✬✬";
	g_sCTRanks[6]  = "✬✬✬";
	g_sCTRanks[7]  = "★";
	g_sCTRanks[8]  = "★★";
	g_sCTRanks[9]  = "★★★";
	g_sCTRanks[10] = "✪";
	g_sCTRanks[11] = "✪✪";
	g_sCTRanks[12] = "✪✪✪";
	g_sCTRanks[13] = "✸";
	g_sCTRanks[14] = "✸✸";
	g_sCTRanks[15] = "✸✸✸";
	
	g_sTRanks[0]  = "♟";
	g_sTRanks[1]  = "♟";
	g_sTRanks[2]  = "♟♟";
	g_sTRanks[3]  = "♟♟♟";
	g_sTRanks[4]  = "♗";
	g_sTRanks[5]  = "♗♗";
	g_sTRanks[6]  = "♗♗♗";
	g_sTRanks[7]  = "♜";
	g_sTRanks[8]  = "♜♜";
	g_sTRanks[9]  = "♜♜♜";
	g_sTRanks[10] = "♛";
	g_sTRanks[11] = "♛♛";
	g_sTRanks[12] = "♛♛♛";
	g_sTRanks[13] = "♚";
	g_sTRanks[14] = "♚♚";
	g_sTRanks[15] = "♚♚♚";
	//ServerCommand("sm plugins reload hide_attacker");
}

public void OnPluginEnd()
{
	for(int i=1; i<=MaxClients; i++)
		if(IsClientInGame(i))
			OnClientDisconnect(i);
	SaveMapWinLose();
}

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("GetRank",			NativeGetRank);				// ранк игрока (client, команда)
	RegPluginLibrary("jail_stats");
	return APLRes_Success;
}

public int NativeGetRank(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	int iTeam = GetNativeCell(2);
	if(IsClientInGame(iClient)) {
		if(iTeam == 2)			return g_iPointsT[iClient];
		if(iTeam == 3)			return g_iPointsCT[iClient];
		else ThrowNativeError(SP_ERROR_INDEX, "Team %i is invalid", iTeam);
	} else ThrowNativeError(SP_ERROR_INDEX, "Client index %i is invalid", iClient);
	return -1;
}

// вычисление ЛВЛ
#define MAX_LVL			15		// кол-во значков у КТ и Т (длинна массива)

// На сколько делить Points для получения ... (а потом в кв.корень)
#define TX0				16.5	// ... значка за Т
#define TX1				4		// ... LVL from T
#define CTX0			22		// ... значка за CТ
#define CTX1			5.5		// ... LVL from CT

public Action Update (Handle timer)
{
	char sClan[92];
	int iRank[2];			// 0 - какой значок, 1 - цифра ранга 0-80
	for (int i=1; i<=MaxClients; i++)
		if (IsClientInGame(i) && !IsFakeClient(i)) {
			if (GetClientTeam(i) == 2) {
				CS_SetClientContributionScore(i, g_iPointsT[i]);
				if (g_iPointsT[i] > 0) {
					iRank[0] = RoundToFloor( SquareRoot(float(g_iPointsT[i]))/TX0 );
					iRank[1] = RoundToFloor( SquareRoot(float(g_iPointsT[i]))/TX1 );
				} else iRank = {0, 0};
				if (iRank[0] > MAX_LVL)
					iRank[0] = MAX_LVL;
				if (SteamWorks_HasLicenseForApp(i, 730) != k_EUserHasLicenseResultHasLicense)
					Format(sClan, sizeof(sClan), "%i_%s[P]", iRank[1], g_sTRanks[iRank[0]]);
				else if (SteamWorks_HasLicenseForApp(i, 624820))
					Format(sClan, sizeof(sClan), "%i_%s[f]", iRank[1], g_sTRanks[iRank[0]]);
				else Format(sClan, sizeof(sClan), "%i_%s", iRank[1], g_sTRanks[iRank[0]]);
				CS_SetClientClanTag(i, sClan);
			} else if (GetClientTeam(i) == 3) {
				CS_SetClientContributionScore(i, g_iPointsCT[i]);
				if (g_iPointsCT[i] > 0) {
					iRank[0] = RoundToFloor( SquareRoot(float(g_iPointsCT[i]))/CTX0 );
					iRank[1] = RoundToFloor( SquareRoot(float(g_iPointsCT[i]))/CTX1 );
				} else iRank = {0, 0};
				if (iRank[0] > MAX_LVL)
					iRank[0] = MAX_LVL;
				if (SteamWorks_HasLicenseForApp(i, 730) != k_EUserHasLicenseResultHasLicense)
					Format(sClan, sizeof(sClan), "%i_%s[P]", iRank[1], g_sCTRanks[iRank[0]]);
				else if (SteamWorks_HasLicenseForApp(i, 624820))
					Format(sClan, sizeof(sClan), "%i_%s[f]", iRank[1], g_sCTRanks[iRank[0]]);
				else Format(sClan, sizeof(sClan), "%i_%s", iRank[1], g_sCTRanks[iRank[0]]);
				CS_SetClientClanTag(i, sClan);
			}
		} else ResetPoints(i);
}

// выполняется каждый раз после смены карты
public void OnMapStart()
{
	// получаем количество побед, поражений для текущей карты
	char szQuery[256];
	g_iMapWin = 0;
	g_iMapLose = 0;
	GetCurrentMap(g_szCurrentMap, sizeof(g_szCurrentMap));
	FormatEx(szQuery, sizeof(szQuery), "SELECT `win`, `lose` FROM `maps` where `mapname` = '%s' LIMIT 1;", g_szCurrentMap);
	dbgMsg("получаем количество побед, поражений для %s", g_szCurrentMap);
	
	g_hDatabase.Query(SQLT_OnMapStart, szQuery);
	g_fMapWinLoseIsSent = false;
	dbgMsg("Карта %s", g_szCurrentMap);
}

// при получении текущих количества побед, поражений карты
public void SQLT_OnMapStart(Database hDatabase, DBResultSet hQuery, const char[] szError, any data)
{
	// если запрос не прошел (ошибка)
	if ( hQuery == null ) {
		LogError("SQLT_OnMapStart error: \"%s\"", szError);
		SetFailState("SQLT_OnMapStart error: \"%s\"", szError);
	}
	// если данные найдены
	if ( hQuery.FetchRow() ) {
		// извлекаем
		g_iMapLose = hQuery.FetchInt(1);
		// проверка на деление на ноль!
		if ( g_iMapLose == 0 ) {
			g_iMapWin = 0;
			g_fMapCoef = 1.0;
		} else {
			g_iMapWin = hQuery.FetchInt(0);
			g_fMapCoef = g_iMapLose / float(g_iMapWin);
		}
	} else {	// если данных нет
		// записываем новые
		char szQuery[256];
		FormatEx(szQuery, sizeof(szQuery), "INSERT INTO `maps` (`mapname`, `win`, `lose`) VALUES ('%s', 1, 1);", g_szCurrentMap);
		dbgMsg("если данных нет, записываем новые Карта - %s", g_szCurrentMap);
		g_hDatabase.Query(SQLT_OnMapsInsert, szQuery);
		g_fMapCoef = 1.0;
	}
}

// вставка данных о новой карте в бд
public void SQLT_OnMapsInsert(Database hDatabase, DBResultSet hQuery, const char[] szError, any data)
{
	if ( hQuery == null ) {
		LogError("SQLT_OnMapsInsert error: \"%s\"", szError);
		SetFailState("SQLT_OnMapsInsert error: \"%s\"", szError);
	}
}

// вызывается каждый раз, когда карта заканчивается
public void Event_WinMapMatch(Handle event, const char[] name, bool DontB)
{
	g_fMapWinLoseIsSent = true;
	SaveMapWinLose();
}

public void OnMapEnd()
{
	if (!g_fMapWinLoseIsSent)
		SaveMapWinLose();
}

void SaveMapWinLose()
{	// вызывается в конце карты
	char szQuery[256];
	FormatEx(szQuery, sizeof(szQuery), "UPDATE `maps` SET `win` = %d, `lose` = %d WHERE `mapname` = '%s'", g_iMapWin, g_iMapLose, g_szCurrentMap);
	dbgMsg("вызывается каждый раз, когда карта заканчивается UPDATE `maps`  `win` = %d, `lose` = %d WHERE `mapname` = '%s'", g_iMapWin, g_iMapLose, g_szCurrentMap);
	g_hDatabase.Query(SQLT_OnMapsUpdate, szQuery);
}

// при обновлении данных в бд
public void SQLT_OnMapsUpdate(Database hDatabase, DBResultSet hQuery, const char[] szError, any data)
{
	if ( hQuery == null )
		LogError("SQLT_OnMapsUpdate error: \"%s\"", szError);
}

public Action Command_Give(int client, int args)
{
	if (args < 3) {
		ReplyToCommand(client, "[SM] Usage: sm_js_givepts <steamid> <team 2-t or 3-ct> <pts>");
		return Plugin_Handled;
	}
	char Arguments[256];
	GetCmdArgString(Arguments, sizeof(Arguments));
	char szAuth[65];
	int len = BreakString(Arguments, szAuth, sizeof(szAuth));
	char szQuery[256], szName[] = "console";
	char sTeam[6];
	int next_len = BreakString(Arguments[len], sTeam, sizeof(sTeam));
	len += next_len;
	int iTeam = StringToInt(sTeam, sizeof(sTeam));
	if (iTeam == 2)
		Format(sTeam, sizeof(sTeam), "ptsT");
	else if (iTeam == 3)
		Format(sTeam, sizeof(sTeam), "ptsCT");
	else {
		ReplyToCommand(client, "[SM] Usage: sm_js_givepts <steamid> <INVALID TEAM use 2-t or 3-ct> <pts>");
		return Plugin_Handled;
	}
	char sPoints[16];
	next_len = BreakString(Arguments[len], sPoints, sizeof(sPoints));
	int iPoints = StringToInt(sPoints, sizeof(sPoints));
	if(0>= iPoints >1000000) {
		ReplyToCommand(client, "[SM] Usage: sm_js_givepts <steamid> <team 2-t or 3-ct> <INVALID pts 0< iPoints <1000000");
		return Plugin_Handled;
	}
	FormatEx(szQuery, sizeof(szQuery), "UPDATE `clients` SET `%s` = '%s', `name` = '%s' WHERE `auth` = '%s'", sTeam, sPoints, szName, szAuth);
	dbgMsg("отправим данные %s= %s, name= %s WHERE auth = %s", sTeam, sPoints, szName, szAuth);
	g_hDatabase.Query(SQLT_SavePoints, szQuery);

	return Plugin_Handled;
}

public Action Command_Get(int client, int args)
{
	if (args < 1) {
		ReplyToCommand(client, "[SM] Usage: sm_js_getpts <steamid>");
		return Plugin_Handled;
	}
	char Arguments[256];
	GetCmdArgString(Arguments, sizeof(Arguments));
	char szAuth[65];
	BreakString(Arguments, szAuth, sizeof(szAuth));
	char szQuery[256];
	FormatEx(szQuery, sizeof(szQuery), "SELECT `ptsCT`, `ptsT` FROM `clients` WHERE `auth` = '%s' LIMIT 1;", szAuth);
	g_hDatabase.Query(SQLT_Command_Get, szQuery, client);
	return Plugin_Handled;
}

public void SQLT_Command_Get(Database hDatabase, DBResultSet hQuery, const char[] szError, any iClient)
{	// при получении ответа от бд на данные игрока
	if ( hQuery == null )
		LogError("SQLT_OnClientPutInServer error: \"%s\"", szError);
	// если данные есть, то получаем
	if (!iClient || IsClientInGame(iClient)) {
		if (hQuery.FetchRow()) {
			int iPointsCT = hQuery.FetchInt(0);
			int iPointsT = hQuery.FetchInt(1);
			dbgMsg("ответ с БД iPointsCT - %i iPointsT - %i", iPointsCT, iPointsT);
			PrintToServer("ответ с БД iPointsCT - %i iPointsT - %i", iPointsCT, iPointsT);
		} 
	}
}

// на данный момент не используется
//public void OnConfigsExecuted() 
//{
//	
//}

//public void Event_PlayerConnect(Handle event, const char[] name, bool DB)		// выходит ошибка с UID т.к. по идее игрок ещё не в игре 
//{	// первичный запрос
//	int iClient = GetEventInt(event, "index") + 1;
//	GetEventString(event, "networkid", g_szAuth[iClient], sizeof(g_szAuth[]));
//	Call(iClient);
//	dbgMsg("%N подключился (%s) (Event_PlayerConnect)", iClient, g_szAuth[iClient]);
//}

public void OnClientPutInServer(int iClient)
{	// получаем стим, отправляем запрос на очки
	if (g_iPointsCT[iClient] == -1 && IsClientInGame(iClient)) {
		GetClientAuthId(iClient, AuthId_Steam2, g_szAuth[iClient], sizeof(g_szAuth[]));
		Call(iClient);
		CreateTimer(6.0, ReCall, UID(iClient));
		dbgMsg("%N подключился (%s) (OnClientPutInServer)", iClient, g_szAuth[iClient]);
	}
}

public Action ReCall (Handle timer, any iUserId)
{
	int iClient = CID(iUserId);
	// проверка на пропажу очков
	if ((g_iPointsCT[iClient] == -1 || g_iPointsT[iClient] == -1) && iClient && !IsFakeClient(iClient)) {
		GetClientAuthId(iClient, AuthId_Steam2, g_szAuth[iClient], sizeof(g_szAuth[]));
		dbgMsg("Ошибка. %N оказался в игре и без очков!!!!! Берем очки.", iClient);
		Call(iClient);
		CreateTimer(5.0, ReCall, UID(iClient));
	}
}

void Call(int iClient)
{
	char szQuery[256];
	FormatEx(szQuery, sizeof(szQuery), "SELECT `ptsCT`, `ptsT` FROM `clients` WHERE `auth` = '%s' LIMIT 1;", g_szAuth[iClient]);
	g_hDatabase.Query(SQLT_OnClientPutInServer, szQuery, UID(iClient));
}

public void SQLT_OnClientPutInServer(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{	// при получении ответа от бд на данные игрока
	if ( hQuery == null )
		LogError("SQLT_OnClientPutInServer error: \"%s\"", szError);
	// если данные есть, то получаем, иначе вставляем нули в бд
	int iClient = CID(iUserId);
	if (iClient) {
		if ( hQuery.FetchRow() ) {
			g_iPointsCT[iClient] = hQuery.FetchInt(0);
			g_iPointsT[iClient] = hQuery.FetchInt(1);
			dbgMsg("ответ с БД g_iPointsCT[%N] - %i g_iPointsT[%N] - %i", iClient, g_iPointsCT[iClient], iClient, g_iPointsT[iClient]);
		} else {
			char szQuery[256], szName[32], szEscapedName[64];
			GetClientName(iClient, szName, sizeof(szName));
			// экранирование ника, чтобы запрос успешно прошел, и защититься от SQL инъекции (мало ли)
			SQL_EscapeString(g_hDatabase, szName, szEscapedName, sizeof(szEscapedName));
			// если запись уже есть, то sqlite не будет вставлять нули.
			FormatEx(szQuery, sizeof(szQuery), "INSERT INTO `clients` (`auth`, `ptsT`, `ptsCT`, `name`) SELECT '%s', 0, 0, '%s' WHERE NOT EXISTS (SELECT `auth` FROM `clients` WHERE `auth` = '%s');", g_szAuth[iClient], szEscapedName, g_szAuth[iClient]);
			g_hDatabase.Query(SQLT_OnInsertClient, szQuery);
			dbgMsg("Добавим %N в БД", iClient);
			
			// все данные на ноль
			g_iPointsCT[iClient] = 0;
			g_iPointsT[iClient] = 0;
		}
	} else dbgMsg("ОШИБКА ответ с БД пришел, но игрок вышел");
}

// при вставке клиента
public void SQLT_OnInsertClient(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if ( hQuery == null )
		LogError("SQLT_OnInsertClient error: \"%s\"", szError);
}

// при отключении клиента
public void OnClientDisconnect(int iClient)
{
	SavePoints(iClient);
	dbgMsg("%N OnClientDisconnect (очки = -1)", iClient);
	ResetPoints(iClient);
}

void ResetPoints(int iClient)
{
	g_iPointsT[iClient] = -1;
	g_iPointsCT[iClient] = -1;
	Format(g_szAuth[iClient], sizeof(g_szAuth[]), "");
}

void SavePoints(int iClient) {
	if (g_iPointsT[iClient] <=0 && g_iPointsCT[iClient] <=0) {
		dbgMsg("Отправлять нечего ptsT= %d, ptsCT= %d, auth = %s", g_iPointsT[iClient], g_iPointsCT[iClient], g_szAuth[iClient]);
		return;
	}
	char szQuery[256], szName[32], szEscapedName[64];
	GetClientName(iClient, szName, sizeof(szName));
	SQL_EscapeString(g_hDatabase, szName, szEscapedName, sizeof(szEscapedName));
	FormatEx(szQuery, sizeof(szQuery), "UPDATE `clients` SET `ptsT` = %d, `ptsCT` = %d, `name` = '%s' WHERE `auth` = '%s'", g_iPointsT[iClient], g_iPointsCT[iClient], szEscapedName, g_szAuth[iClient]);
	dbgMsg("отправим данные ptsT= %d, ptsCT= %d name= %s WHERE auth = %s", g_iPointsT[iClient], g_iPointsCT[iClient], szEscapedName, g_szAuth[iClient]);
	g_hDatabase.Query(SQLT_SavePoints, szQuery);
}

// при отключении клиента, SQL запрос
public void SQLT_SavePoints(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if ( hQuery == null )
		LogError("SQLT_SavePoints error: \"%s\"", szError);
}

// игрок возродился первый раз
public void Event_player_spawned(Event hEvent, const char[] szEventName, bool bDontBroadcast)
{
	int iClient = CID(hEvent.GetInt("userid"));
	float fRank[2];			// 0 - какой значок, 1 - цифра ранга 0-80
	if (GetClientTeam(iClient) == 3) {
		if (g_iPointsCT[iClient] > 0) {
			fRank[0] = SquareRoot(float(g_iPointsCT[iClient]))/CTX0;
			fRank[1] = SquareRoot(float(g_iPointsCT[iClient]))/CTX1;
			int iRank = RoundToFloor(fRank[0]);
			float fToNextPts1 = Pow((fRank[1]+1.0) * CTX1, 2.0) - g_iPointsCT[iClient];
			if (iRank <= MAX_LVL)
				CGOPrintToChat(iClient, "{BLUE}[CT] {DEFAULT}[%s] %.0f {GREEN}ЛВЛ.  До {DEFAULT}%.0f {GREEN}ЛВЛ осталось {DEFAULT}%.0f {GREEN}очков", g_sCTRanks[iRank], fRank[1], fRank[1] + 1.0, fToNextPts1);
			if (fRank[0] < float(MAX_LVL)) {
				float fToNextPts0 = Pow((fRank[0]+1.0) * CTX0, 2.0) - g_iPointsCT[iClient];
				CGOPrintToChat(iClient, "{BLUE}[CT] {GREEN}До звания {DEFAULT}[%s] {GREEN}осталось {DEFAULT}%.0f {GREEN}очков", g_sCTRanks[iRank+1], fToNextPts0, fRank[1]);
			} else CGOPrintToChat(iClient, "{BLUE}[CT] {GREEN}Звание {DEFAULT}[%s] {GREEN}максимальное", g_sCTRanks[iRank]);
		}
		if (g_iPointsCT[iClient] < 100) {
			CGOPrintToChat(iClient, "{GREEN}На сервере действует уникальная система очков для JAIL сервера");
			CGOPrintToChat(iClient, "{GREEN}пиши {BLUE}!jailinfo {GREEN}для подробной информации");
		}
	} else if (GetClientTeam(iClient) == 2) {
		if (g_iPointsT[iClient] > 0) {
			fRank[0] = SquareRoot(float(g_iPointsT[iClient]))/TX0;
			fRank[1] = SquareRoot(float(g_iPointsT[iClient]))/TX1;
			int iRank = RoundToFloor(fRank[0]);
			float fToNextPts1 = Pow((fRank[1]+1.0) * TX1, 2.0) - g_iPointsT[iClient];
			if (iRank <= MAX_LVL)
				CGOPrintToChat(iClient, "{RED}[T] {DEFAULT}[%s] %.0f {GREEN}ЛВЛ.  До {DEFAULT}%.0f {GREEN}ЛВЛ осталось {DEFAULT}%.0f {GREEN}очков", g_sTRanks[iRank], fRank[1], fRank[1] + 1.0, fToNextPts1);
			if (fRank[0] < float(MAX_LVL)) {
				float fToNextPts0 = Pow((fRank[0]+1.0) * TX0, 2.0) - g_iPointsT[iClient];
				CGOPrintToChat(iClient, "{RED}[T] {GREEN}До звания {DEFAULT}[%s] {GREEN}осталось {DEFAULT}%.0f {GREEN}очков", g_sTRanks[iRank+1], fToNextPts0, fRank[1]);
			} else CGOPrintToChat(iClient, "{RED}[T] {GREEN}Звание {DEFAULT}[%s] {GREEN}максимальное", g_sTRanks[iRank]);
		}
		if (g_iPointsT[iClient] < 100) {
			CGOPrintToChat(iClient, "{GREEN}На сервере действует уникальная система очков для JAIL сервера");
			CGOPrintToChat(iClient, "{GREEN}пиши {BLUE}!jailinfo {GREEN}для подробной информации");
		}
	}
}

// игрок умер
public void Event_PlayerDeath(Event hEvent, const char[] szEventName, bool bDontBroadcast)
{
	// проверка условия на количество игроков
	if (!CheckCondition())
		return;
	
	// если лр не был включен
	if (!IsLrActivated()) {
		// получаем убитого
		int iVictim = CID(hEvent.GetInt("userid"));
		
		// получаем убийцу
		int iAttacker = CID(hEvent.GetInt("attacker"));
		// если убил сам себя - ничего не делаем
		if ( iAttacker == iVictim ) return;
		
		// если убийца Т и жертва - СТ
		if ( iAttacker && GetClientTeam(iAttacker) == 2 && GetClientTeam(iVictim) == 3  && !IsFakeClient(iAttacker)) {
			// смотрим на оружие, с которого убили
			char szWeapon[32]; hEvent.GetString("weapon", szWeapon, sizeof(szWeapon));
			int iBonus = 1;
			if ( StrContains(szWeapon, "knife", false) != -1 ) {
				CGOPrintToChat(iAttacker, "{RED}[T] {GREEN}Убийство с ножа {PURPLE}x%d", KNIFE_COEF);
				dbgMsg( "%N - Убийство с ножа x%d", iAttacker, KNIFE_COEF);
				iBonus = KNIFE_COEF;
			} else if ( StrContains(szWeapon, "molotov", false) != -1 || StrContains(szWeapon, "hegrenade", false) != -1 ) {
				CGOPrintToChat(iAttacker, "{RED}[T] {GREEN}Убийство с гранаты {PURPLE}x%d", HE_COEF);
				dbgMsg( "%N - Убийство с гранаты x%d", iAttacker, HE_COEF);
				iBonus = HE_COEF;
			}
			
			// получаем число Т, КТ
			//static float fCoefs[] = {1.0, 1.3, 1.2, 1.1, 1.0};
			
			// является ли убийство фб
			if ( !g_bFirstBlood ) {
				CGOPrintToChat(iAttacker, "{RED}[T] {GREEN}Firstblood {PURPLE}x%d", F_BLOOD);
				dbgMsg( "%N - FirstbloodCT x%d", iAttacker, F_BLOOD);
				iBonus += F_BLOOD;
				g_bFirstBlood = true;
			}
			
			// подсчет и выдача очк.
			//if ( iCTCount > 4 ) iCTCount = 4;
			float fCountCoef = GetCountCoef();
			float fTimeCoef = GetRoundTime()/240.0;			// в первые 3 минуты будет давать меньше очков (защита от фарма)
			if (fTimeCoef > 1.0)			fTimeCoef = 1.0;
			
			int iPointsT = RoundToNearest(T_KILL_COEF * iBonus * fTimeCoef / fCountCoef / g_fMapCoef);
			g_iPointsT[iAttacker] += iPointsT;
			if (0<iPointsT<2000) {
				Shop_GiveClientCredits(iAttacker, iPointsT, -1);		// -1 - CREDITS_BY_NATIVE
				dbgMsg("%N (команда %d) получил %d очк. за убийство %N (команда %d)", iAttacker, GetClientTeam(iAttacker), iPointsT, iVictim, GetClientTeam(iVictim));
				dbgMsg("%d * %d * %.2f / %.2f / %.2f  = %d (%d/%d)", T_KILL_COEF, iBonus, fTimeCoef, fCountCoef, g_fMapCoef, iPointsT, GetTCount(), GetCTCount() );
				CGOPrintToChat(iAttacker, "{RED}[T] {GREEN}Ты получил {PURPLE}%i {GREEN}очков за убийство {DEFAULT}(КТ/Т - x%.1f , map - x%.1f)", iPointsT, fCountCoef, g_fMapCoef);
				
				PrintHintText(iAttacker, "<font color='#00ff00'><font color='#ffd700'>+ %d кредитов</font></font>", iPointsT);
				ClientCommand(iAttacker, "play physics/metal/chain_impact_soft2.wav");
				CGOPrintToChatAll("{RED}[T] {GREEN}За убийство %N [?T?] получил {DEFAULT}%d {GREEN}очк.", iVictim, iPointsT);
				SavePoints(iAttacker);			// сохранение данных в бд
			
				// то же самое, только для ассистента в убийстве
				int iAssister = GetClientOfUserId(hEvent.GetInt("assister"));
				if (iAssister && !IsFakeClient(iAssister)) {
					iPointsT = RoundToFloor(iPointsT / 2.0);
					Shop_GiveClientCredits(iAssister, RoundToNearest(iPointsT/2.0), -1);		// -1 - CREDITS_BY_NATIVE
					if (RoundToNearest(iPointsT/2.0) > 0)
						ClientCommand(iAssister, "play physics/metal/chain_impact_soft2.wav");
					g_iPointsT[iAssister] += iPointsT;
					SavePoints(iAssister);
					dbgMsg("%N (команда %d) получил %d очк. за ассист в убийстве %N (команда %d)", iAssister, GetClientTeam(iAssister), iPointsT, iVictim, GetClientTeam(iVictim));
					CGOPrintToChatAll( "{RED}[T] {GREEN}За ассист в убийстве %N [T??] получил {DEFAULT}%d {GREEN}очк.", iVictim, iPointsT);
				}
			} else if (iPointsT != 0)
				dbgMsg("Ошибка в вычислениях, iPointsT = %d", iPointsT);
		}
		// иначе если жертва Т
		else if ( GetClientTeam(iVictim) == 2 && !IsFakeClient(iVictim)) {
			g_iTDies++;
			// получаем количество Т, живых Т, КТ
			int iTAliveCount = GetAliveTCount();
			// выдаем очки 
			if ( GetCTCount() < 1 ) return;
			int iPointsTrnd = RoundToNearest(T_ROUND_COEF*Pow(float(g_iTCount30-iTAliveCount), 1.8));
			if (0<iPointsTrnd<2000) {
				dbgMsg("%N получил %d очк. после смерти (всего %d Т, из них %d осталось живых)", iVictim, iPointsTrnd, g_iTCount30, iTAliveCount);
				//dbgMsg("%d*(%i-%i) / (%d/%i)", T_ROUND_COEF, g_iTCount30, iTAliveCount, T_MAX, g_iTCount30);
				CGOPrintToChat(iVictim, "{GREEN}Ты получил {PURPLE}%d {GREEN}кр., т.к. умер {PURPLE}%dм {GREEN}по счету", iPointsTrnd, g_iTCount30-iTAliveCount);
				CGOPrintToChat(iVictim, "{GREEN}Чем раньше умираешь тем меньше кредитов получаешь");
				PrintHintText(iVictim, "<font color='#ffd700'>+ %d кредитов</font>", iPointsTrnd );
				ClientCommand(iVictim, "play physics/metal/chain_impact_soft2.wav");
				Shop_GiveClientCredits(iVictim, iPointsTrnd, -1);		// -1 - CREDITS_BY_NATIVE
			} else if (iPointsTrnd != 0)
				dbgMsg("Ошибка в вычислениях, iPointsTrnd = %d", iPointsTrnd);
		}
	}
}

float GetCountCoef()
{
	float fTCount, fCTCount, fCountCoef;
	for ( int i=1; i <= MaxClients; ++i )
	if ( IsClientInGame(i) && g_bJoined[i] ) {
		int iTeam = GetClientTeam(i);
		if ( iTeam == 2 )
			fTCount+=1.0+(((g_iPointsT[i]>0)? g_iPointsT[i]:1)/60000.0);
		else if ( iTeam == 3 )
			fCTCount+=1.0+(((g_iPointsCT[i]>0)? g_iPointsCT[i]:1)/80000.0);
	}
	fCountCoef = fTCount/fCTCount;
	if (fCountCoef<0.5)			fCountCoef = 0.5;
	else if (fCountCoef>10.0)	fCountCoef = 10.0;
	return fCountCoef;
}

int GetCTCount()
{
	int iCTCount=0;
	for (int i=1; i <= MaxClients; ++i )
		if ( IsClientInGame(i) && g_bJoined[i] && GetClientTeam(i) == 3 )
			iCTCount++;
	return iCTCount;
}

int GetTCount()
{
	int iTCount=0;
	for (int i=1; i <= MaxClients; ++i )
		if ( IsClientInGame(i) && g_bJoined[i] && GetClientTeam(i) == 2 )
			iTCount++;
	return iTCount;
}

int GetAliveTCount()
{
	int iTAliveCount=0;
	for ( int i = 1; i <= MaxClients; ++i )
		if ( IsClientInGame(i) && IsPlayerAlive(i) && !IsPlayerGhost(i) && GetClientTeam(i) == 2 )
			iTAliveCount++;
	return iTAliveCount;
}

int GetRoundTime()
{
	dbgMsg("GetTime(%i) - g_iRoundStartTime %i = GetRoundTime(%i)", GetTime(), g_iRoundStartTime, GetTime() - g_iRoundStartTime);
	return (GetTime() - g_iRoundStartTime);
}

// начало раунда
public void Event_RoundStart(Event hEvent, const char[] szEventName, bool bDontBroadcast)
{
	// ждем 30 секунд, чтобы получить подключенных игроков
	CreateTimer(30.0, Timer_GetJoinedPlayers);
	// фб не было сделано
	g_iTCount30 = 0;
	g_bFirstBlood = false;
	g_iTDies = 0;
	g_iGatherPoints = 0;
	g_iRoundStartTime = GetTime();
	for ( int i=1; i<=MaxClients; ++i )
		g_bJoined[i] = ( IsClientInGame(i) && GetClientTeam(i) > 1 ) ? true : false;
	CGOPrintToChatAll("{OLIVE}[JAIL-STATS] {BLUE}Карта %s{GREEN}, сложность для КТ {PURPLE}%.2f", g_szCurrentMap, g_fMapCoef);
}

// получаем подключенных игроков через 30 секунд
public Action Timer_GetJoinedPlayers(Handle hTimer)
{
	g_iTCount30 = GetTCount();
	for ( int i = 1; i <= MaxClients; ++i )
		g_bJoined[i] = ( IsClientInGame(i) && GetClientTeam(i) > 1 ) ? true : false;
}

public void everyone_is_here()
{
	if (!CheckCondition()) {
		CGOPrintToChatAll("{OLIVE}[JAIL-STATS] {GREEN}Т должно быть не меньше %i чтобы получать очки", MIN_T);
		dbgMsg("Т должно быть не меньше %i чтобы получать очки", MIN_T);
		return;
	}
	CGOPrintToChatAll("{OLIVE}[JAIL-STATS] {GREEN}Все Т собраны");
	if (!IsLrActivated()) {
		// подсчет очк.
		float fCountCoef = GetCountCoef();
		float fTimeCoef = 90.0/GetRoundTime();
		int iTCount = GetTCount();
		float fDiedCoef;
		if(iTCount>=g_iTDies)
			fDiedCoef = float(iTCount-g_iTDies)/iTCount;
		else fDiedCoef = 0.1;
		if (!fCountCoef || !g_fMapCoef || !fTimeCoef || !fDiedCoef || !iCT2_COEF)
			return;
		g_iGatherPoints = RoundToNearest(fCountCoef*g_fMapCoef*fTimeCoef*fDiedCoef*iCT2_COEF);
		dbgMsg("iTCount - %i g_iTDies - %i  fDiedCoef - %.2f ", iTCount, g_iTDies,  fDiedCoef);
		dbgMsg("В конце раунда КТ получат по %d очк. за то что сборали всех Т", g_iGatherPoints);
		dbgMsg("fCountCoef = %.2f, g_fMapCoef = %.2f, fTimeCoef = %.2f, fDiedCoef = %.2f, iCT2_COEF = %d", fCountCoef, g_fMapCoef, fTimeCoef, fDiedCoef, iCT2_COEF);
		if (-1000 < g_iGatherPoints < 1000) {
			for ( int i=1; i<=MaxClients; ++i )
				if ( IsClientInGame(i) ) {
					if (IsPlayerAlive(i) && !IsPlayerGhost(i) && GetClientTeam(i) == 3) {
						dbgMsg("%N получит %d очк. за сбор всех Т", i, g_iGatherPoints);
						if (g_iGatherPoints > 0) {
							CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}В конце раунда ты получишь {BLUE}%d {GREEN}очк. (или менее) за то что охрана сборала всех Т", g_iGatherPoints);
							CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}За скорость - {BLUE}х%.2f{GREEN}, за сохранность состава - {BLUE}х%.2f", fTimeCoef, fDiedCoef);
							PrintHintText(i, "<font color='#ffd700'>+ %d кредитов (в конце раунда)</font>", g_iGatherPoints);
							ClientCommand(i, "play physics/metal/chain_impact_soft2.wav");
						}
					} else CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}В конце раунда охранники получат по {BLUE}%d {GREEN}очк. за сбор всех Т", g_iGatherPoints);
				}
		} else  {
			dbgMsg("Ошибка в подсчетде g_iGatherPoints = %d", g_iGatherPoints);
			g_iGatherPoints = 0;
		}
	}
}

// в конце раунда
public void Event_RoundEnd(Event hEvent, const char[] szEventName, bool bDontBroadcast)
{
	// проверка условий
	if (!CheckCondition()) {
		CGOPrintToChatAll("{OLIVE}[JAIL-STATS] {GREEN}Т должно быть не меньше %i чтобы получать очки", MIN_T);
		dbgMsg("Т должно быть не меньше %i чтобы получать очки", MIN_T);
		return;
	}
	// выдача очк., если выиграли КТ без ЛР или же лр был включен и выйграла любая команда
	if (!IsLrActivated() && hEvent.GetInt("winner") == 3) {
		
		GiveCT_Points();
		// увеличение числа побед на карте
		g_iMapWin++;
	} else {
		// КТ проиграли (если не в ЛР), увеличение числа поражений на карте
		IsLrActivated() ? g_iMapWin++ : g_iMapLose++;
		
		// выдача каждому живому Т очк.
		// выдаем очки Т
		if (GetCTCount() && g_iTCount30) {
			int iPointsT = RoundToNearest(T_ROUND_COEF*Pow(float(g_iTCount30-GetAliveTCount()), 1.8));
			for ( int i=1; i <= MaxClients; ++i )
				if ( g_bJoined[i] && IsClientInGame(i) && IsPlayerAlive(i) && !IsPlayerGhost(i) && GetClientTeam(i) == 2 && !IsFakeClient(i)) {
					if (-20>iPointsT > 2000)
						dbgMsg("Ошибка в подсчетде iPointsT = %d", iPointsT);
					else if (iPointsT > 0) {
						CGOPrintToChat(i, "{RED}[Т] {GREEN}Ты получил {RED}%d {GREEN} кр. за победу в раунде", iPointsT);
						//PrintHintText(i, "<font color='#ffd700'>+ %d кредитов</font>", iPointsT);
						Shop_GiveClientCredits(i, iPointsT, -1);		// -1 - CREDITS_BY_NATIVE
						//ClientCommand(i, "play physics/metal/chain_impact_soft2.wav");
						g_iPointsT[i] += iPointsT;
						SavePoints(i);
					}
				}
		}
	}
	dbgMsg("Конец раунда");
}

// проверка условий
bool CheckCondition()
{
	int iCount = 0;
	for (int i=1; i<=MaxClients; ++i)
		if (IsClientInGame(i) && GetClientTeam(i) == 2 && g_bJoined[i])
			iCount++;
	return iCount >= MIN_T;
}

// при начале LR
public void t2lrcfg_OnLrStarted()
{
	// проверка условий
	if (!CheckCondition()) {
		CGOPrintToChatAll("{OLIVE}[JAIL-STATS] {GREEN}Т должно быть не меньше %i чтобы получать очки", MIN_T);
		dbgMsg("Т должно быть не меньше %i чтобы получать очки", MIN_T);
		return;
	}
	GiveCT_Points();
}

public void GiveCT_Points()
{
	if (!CheckCondition())
		return;
	// подсчет очк.
	float fCountCoef = GetCountCoef();
	float fTimeCoef = GetRoundTime()/360.0;
	if (fTimeCoef>1.0)			fTimeCoef = 1.0;
	int iPointsCT = RoundToNearest(fCountCoef*g_fMapCoef*fTimeCoef*iCT_COEF);
	int iHalfPointsCT = RoundToFloor(iPointsCT/2.0);
	int iPoints;
	g_iGatherPoints=RoundToNearest(fTimeCoef*g_iGatherPoints);
	for (int i=1; i <= MaxClients; ++i ) {
		if (IsClientInGame(i)) {
			if (g_bJoined[i] && GetClientTeam(i) == 3 && !IsFakeClient(i)) {
				iPoints = IsPlayerAlive(i) ? iPointsCT:iHalfPointsCT;
				CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}Ты получил {BLUE}%d {GREEN}очк. за этот раунд", iPoints);
				if (g_iGatherPoints)
					CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}За сбор Т{BLUE}%d {GREEN}очк. {DEFAULT}(в начале раунда)", g_iGatherPoints);
				if (fTimeCoef<1.0) {
					CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}Рекомендуется проводить раунд не менее {BLUE}%.1f {GREEN}минут для получения максимальных очков", 360.0/60);
					CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}данный раунд длился лишь {BLUE}%.1f {GREEN}минут, поэтому вы получили лишь {BLUE}%.0f%% {GREEN}от положенных очков", GetRoundTime()/60.0, fTimeCoef*100);
				}
				PrintHintText(i, "<font color='#ffd700'>+ %d кредитов</font>", iPoints+g_iGatherPoints);
				if (!IsPlayerAlive(i))
					CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}Вы получили в два раза меньше т.к. не дожили до конца, и охрана выиграла");
				
				// выдача очк.
				if (-1000<iPointsCT<1000  && -1000<g_iGatherPoints<1000 ) {
					dbgMsg("%N получил %d очк. за победу в раунде (%d*%.1f*%.2f*%.2f*%.1f)", i, iPoints, iCT_COEF, fCountCoef, g_fMapCoef, fTimeCoef, (IsPlayerAlive(i)?1.0:0.5));
					Shop_GiveClientCredits(i, iPoints+g_iGatherPoints, -1);		// -1 - CREDITS_BY_NATIVE
					if (iPoints > 0)
						ClientCommand(i, "play physics/metal/chain_impact_soft2.wav");
					g_iPointsCT[i] += (iPoints+g_iGatherPoints);
					// сохранение данных
					SavePoints(i);
				} else dbgMsg("Ошибка в подсчетде iPointsCT = %d", iPointsCT);
			} CGOPrintToChat(i, "{BLUE}[КТ] {GREEN}Охранники получили по {BLUE}%d {GREEN}очк. за победу в раунде {DEFAULT}(CT/T - x%.1f map - x%.2f) \n (мертвые КТ в 2 раза меньше)", iPointsCT, fCountCoef, g_fMapCoef);
		}
	}
	dbgMsg("КТ получили %d очк.", iPointsCT);
}

/*public Action Command_Top(int iClient, int iArgc)
{
	CT(iClient)
	g_hDatabase.Query(SQLT_OnGetTop, "SELECT `pts`, `name` FROM `clients` ORDER BY `pts` DESC LIMIT 10;", UID(iClient));
	return Plugin_Handled;
}*/

// создать топ КТ
void GetCTTop(int iClient)
{
	CT(iClient)
	g_hDatabase.Query(SQLT_OnGetTop, "SELECT `ptsCT`, `name` FROM `clients` ORDER BY `ptsCT` DESC LIMIT 10;", UID(iClient));
}

// создать топ Т
void GetTTop(int iClient)
{
	CT(iClient)
	g_hDatabase.Query(SQLT_OnGetTop, "SELECT `ptsT`, `name` FROM `clients` ORDER BY `ptsT` DESC LIMIT 10;", UID(iClient));
}

// получить топ карт
void GetMapTop(int iClient)
{
	CT(iClient)
	g_hDatabase.Query(SQLT_OnGetMapTop, "SELECT `lose`/(`win`*1.0), `mapname` FROM `maps` ORDER BY `lose`/(`win`*1.0) DESC;", UID(iClient));
}

// при получении данных для топа от бд
public void SQLT_OnGetTop(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if ( hQuery == null )
		LogError("SQLT_OnGetTop error: \"%s\"", szError);
	
	// проверка, на сервере ли запросивший клиент
	int iClient = CID(iUserId);
	if ( !iClient ) return;
	
	// создание меню
	Menu hMenu = new Menu(Handle_Top);
	hMenu.SetTitle("Топ");
	
	// пока есть данные, добавление в меню
	while ( hQuery.FetchRow() ) {
		int iPoints; char szName[32], szBuffer[64];
		iPoints = hQuery.FetchInt(0);
		hQuery.FetchString(1, szName, sizeof(szName));
		FormatEx(szBuffer, sizeof(szBuffer), "%s (%d очк.)", szName, iPoints);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}
	
	// вдруг в бд ничего нет по запросу
	if ( !hMenu.ItemCount )
		hMenu.AddItem(NULL_STRING, "Пусто", ITEMDRAW_DISABLED);
	
	// добавление кнопки "назад", вывод клиенту меню
	hMenu.ExitBackButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// то же самое, но для карт
public void SQLT_OnGetMapTop(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if ( hQuery == null )
		LogError("SQLT_OnGetTop error: \"%s\"", szError);
	
	int iClient = CID(iUserId);
	if ( !iClient ) return;
	
	Menu hMenu = new Menu(Handle_Top);
	hMenu.SetTitle("Топ");
	
	while ( hQuery.FetchRow() ) {
		float fPoints; char szName[32], szBuffer[64];
		fPoints = hQuery.FetchFloat(0);
		hQuery.FetchString(1, szName, sizeof(szName));
		FormatEx(szBuffer, sizeof(szBuffer), "%s - x%.2f", szName, fPoints);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}
	
	if ( !hMenu.ItemCount )
		hMenu.AddItem(NULL_STRING, "Пусто", ITEMDRAW_DISABLED);
	
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// при натажии в топ меню 
public int Handle_Top(Menu hMenu, MenuAction action, int iClient, int iSlot)
{
	if ( action == MenuAction_Cancel && iSlot == MenuCancel_ExitBack )
		Command_JailStats(iClient, 0);
	else if ( action == MenuAction_End )
		delete hMenu;
}

// при вызове команды sm_rank
public Action Command_Rank(int iClient, int iArgc)
{
	CTH(iClient)
	char szQuery[256];
	FormatEx(szQuery, sizeof(szQuery), "SELECT COUNT(*) FROM `clients` WHERE `ptsCT` > %d;", g_iPointsCT[iClient]);
	g_hDatabase.Query(SQLT_OnGetCTRank, szQuery, UID(iClient));
	return Plugin_Handled;
}

// запрос для команды rank, для ранка КТ
public void SQLT_OnGetCTRank(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if ( hQuery == null )
		LogError("SQLT_OnGetCTRank error: \"%s\"", szError);
	
	int iClient = CID(iUserId);
	if ( !iClient ) return;
	
	if ( hQuery.FetchRow() )
		CGOPrintToChat(iClient, "{BLUE}[CT] {GREEN}Ты на {BLUE}%d {GREEN}месте, у тебя {BLUE}%d {GREEN}очк.", hQuery.FetchInt(0), g_iPointsCT[iClient]);
	float fRank[2];			// 0 - какой значок, 1 - цифра ранга 0-80
	if (g_iPointsCT[iClient] > 0) {
		fRank[0] = SquareRoot(float(g_iPointsCT[iClient]))/CTX0;
		fRank[1] = SquareRoot(float(g_iPointsCT[iClient]))/CTX1;
		int iRank = RoundToFloor(fRank[0]);
		float fToNextPts1 = Pow((fRank[1]+1.0) * CTX1, 2.0) - g_iPointsCT[iClient];
		if (iRank <= MAX_LVL)
			CGOPrintToChat(iClient, "{BLUE}[CT] {DEFAULT}[%s] %.0f {GREEN}ЛВЛ.  До {DEFAULT}%.0f {GREEN}ЛВЛ осталось {BLUE}%.0f {GREEN}очков", g_sCTRanks[iRank], fRank[1], fRank[1] + 1.0, fToNextPts1);
		if (fRank[0] < float(MAX_LVL)) {
			float fToNextPts0 = Pow((fRank[0]+1.0) * CTX0, 2.0) - g_iPointsCT[iClient];
			CGOPrintToChat(iClient, "{BLUE}[CT] {GREEN}До звания {DEFAULT}[%s] {GREEN}осталось {BLUE}%.0f {GREEN}очков", g_sCTRanks[iRank+1], fToNextPts0, fRank[1]);
		} else CGOPrintToChat(iClient, "{BLUE}[CT] {GREEN}Звание {DEFAULT}[%s] {GREEN}максимальное", g_sCTRanks[iRank]);
	}
	char szQuery[256];
	FormatEx(szQuery, sizeof(szQuery), "SELECT COUNT(*) FROM `clients` WHERE `ptsT` > %d;", g_iPointsT[iClient]);
	g_hDatabase.Query(SQLT_OnGetTRank, szQuery, UID(iClient));
}

// запрос для команды rank, для ранка Т
public void SQLT_OnGetTRank(Database hDatabase, DBResultSet hQuery, const char[] szError, any iUserId)
{
	if ( hQuery == null )
		LogError("SQLT_OnGetTRank error: \"%s\"", szError);
	
	int iClient = CID(iUserId);
	if ( !iClient ) return;
	
	if ( hQuery.FetchRow() )
		CGOPrintToChat(iClient, "{RED}[T] {GREEN}Ты на {RED}%d {GREEN}месте, у тебя {RED}%d {GREEN}очк.", hQuery.FetchInt(0), g_iPointsT[iClient]);
	float fRank[2];			// 0 - какой значок, 1 - цифра ранга 0-80
	if (g_iPointsT[iClient] > 0) {
		fRank[0] = SquareRoot(float(g_iPointsT[iClient]))/TX0;
		fRank[1] = SquareRoot(float(g_iPointsT[iClient]))/TX1;
		int iRank = RoundToFloor(fRank[0]);
		float fToNextPts1 = Pow((fRank[1]+1.0) * TX1, 2.0) - g_iPointsT[iClient];
		if (iRank <= MAX_LVL)
			CGOPrintToChat(iClient, "{RED}[T] {DEFAULT}[%s] %.0f {GREEN}ЛВЛ.  До {DEFAULT}%.0f {GREEN}ЛВЛ осталось {RED}%.0f {GREEN}очков", g_sTRanks[iRank], fRank[1], fRank[1] + 1.0, fToNextPts1);
		if (fRank[0] < float(MAX_LVL)) {
			float fToNextPts0 = Pow((fRank[0]+1.0) * TX0, 2.0) - g_iPointsT[iClient];
			CGOPrintToChat(iClient, "{RED}[T] {GREEN}До звания {DEFAULT}[%s] {GREEN}осталось {RED}%.0f {GREEN}очков", g_sTRanks[iRank+1], fToNextPts0, fRank[1]);
		} else CGOPrintToChat(iClient, "{RED}[T] {GREEN}Звание {DEFAULT}[%s] {GREEN}максимальное", g_sTRanks[iRank]);
	}
}

// при команде sm_jailstats
public Action Command_JailStats(int iClient, int iArgc)
{
	g_hMenu_Main.Display(iClient, MENU_TIME_FOREVER);
	return Plugin_Handled;
}

// при нажатии в главном меню
public int Handle_MainMenu(Menu hMenu, MenuAction action, int iClient, int iSlot)
{
	if ( action == MenuAction_Select )
		switch (iSlot) {
			case 0: GetCTTop(iClient);
			case 1: GetTTop(iClient);
			case 2: GetMapTop(iClient);
			case 3: {
				Command_Rank(iClient, 0);
				g_hMenu_Main.Display(iClient, MENU_TIME_FOREVER);
			}
			case 4: g_hMenu_Info.Display(iClient, MENU_TIME_FOREVER);
		}
}

// при команде sm_jailinfo
public Action Command_JailInfo(int iClient, int iArgc)
{
	g_hMenu_Info.Display(iClient, MENU_TIME_FOREVER);
	return Plugin_Handled;
}

// при нажатии в INFO меню
public int Handle_InfoMenu(Menu hMenu, MenuAction action, int iClient, int iSlot)
{
	if ( action == MenuAction_Select )
		switch (iSlot) {
			case 0: InfoInfo(iClient);
			case 1: InfoCTpst(iClient);
			case 2: InfoTpst(iClient);
			case 3: InfoTab(iClient);
		}
}

void InfoInfo(int iClient)
{
	Menu menu = new Menu(HandlerInfo);
	menu.SetTitle("Общая информация");
	menu.ExitBackButton = true;
	menu.AddItem(NULL_STRING, "За КТ и Т очки начислются отдельно");
	menu.AddItem(NULL_STRING, "Очки не начисляются если мало игроков");
	menu.AddItem(NULL_STRING, "Очки не начисляются во время ЛР");
	menu.AddItem(NULL_STRING, "Очки зависят от баланса КТ/Т и баланса карты");
	menu.AddItem(NULL_STRING, "Команды: !jailstats или !js, !rank");
	menu.Display(iClient, MENU_TIME_FOREVER);
}

void InfoCTpst(int iClient)
{
	Menu menu = new Menu(HandlerInfo);
	menu.SetTitle("Начисление очков КТ");
	menu.ExitBackButton = true;
	menu.AddItem(NULL_STRING, "Очки за КТ выдаются только за выигрыш раунда");
	menu.AddItem(NULL_STRING, "Активация ЛР тоже считается как победа за КТ");
	menu.AddItem(NULL_STRING, "За убийста Т очков не дается!!!");
	menu.AddItem(NULL_STRING, "Очки зависят от баланса КТ/Т и баланса карты");
	menu.AddItem(NULL_STRING, "Чем сложнее раунд тем больше очков за победу");
	menu.Display(iClient, MENU_TIME_FOREVER);
}

void InfoTpst(int iClient)
{
	Menu menu = new Menu(HandlerInfo);
	menu.SetTitle("Начисление очков Т");
	menu.ExitBackButton = true;
	menu.AddItem(NULL_STRING, "Очки за КТ выдаются только за убийства КТ");
	menu.AddItem(NULL_STRING, "Хотя в конце раунда даётся немножко кредитов");
	menu.AddItem(NULL_STRING, "За разные способы убийства очки умножаются");
	char sBuffer[64];
	Format(sBuffer, sizeof(sBuffer), "За первое убийство x%i, c ножа x%i, гранатой x%i", F_BLOOD, KNIFE_COEF, HE_COEF);
	menu.AddItem(NULL_STRING, sBuffer);
	menu.AddItem(NULL_STRING, "Очки зависят от баланса Т/КТ и баланса карты");
	menu.AddItem(NULL_STRING, "Чем сложнее бунтовать тем больше очков за убийства");
	menu.Display(iClient, MENU_TIME_FOREVER);
}

void InfoTab(int iClient)
{
	Menu menu = new Menu(HandlerInfo);
	menu.SetTitle("Таблица очков TAB");
	menu.ExitBackButton = true;
	menu.AddItem(NULL_STRING, "Клан-тег означает уровень игрока за КТ или Т");
	menu.AddItem(NULL_STRING, "Кол-во денег показывает сколько у игрока кредитов");
	menu.AddItem(NULL_STRING, "Звездочки даются просто за общее время на сервере");
	menu.AddItem(NULL_STRING, "и отображают как долго игрок играет на севрере");
	menu.AddItem(NULL_STRING, "Очки в последнем столбце показывают кол-во очков");
	menu.AddItem(NULL_STRING, "Чем сложнее бунтовать тем больше очков за убийства");
	menu.Display(iClient, MENU_TIME_FOREVER);
}

public int HandlerInfo(Menu menu, MenuAction action, int iClient, int param2)
{
	if(action == MenuAction_Cancel) {
		if(param2 == MenuCancel_ExitBack && g_hMenu_Info)
			g_hMenu_Info.Display(iClient, MENU_TIME_FOREVER);
	} else if(action == MenuAction_End)
		delete menu;
}