#if defined _jail_stats_included
  #endinput
#endif
#define _jail_stats_included

/********************************************************* 
 * Получает Ранк игрока за определенную команду
 * если -1 значит ошибка
 * 
 *********************************************************/ 
native int GetRank(int iClient, int iTeam);
